<?php declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';
unset($argv[0]);

if (!empty($argv) && is_array($argv)) {
	sort($argv);
	try {
		(new \Console\Factory())->find($argv, function () {
			$recursiveDirectory = new RecursiveDirectoryIterator(__DIR__ . '/data');
			return (new RecursiveIteratorIterator($recursiveDirectory, RecursiveIteratorIterator::SELF_FIRST));
		});

	} catch (\Exception $e) {
		echo $e->getMessage();
	}
	die;
}

echo "Request is empty\n\r";

