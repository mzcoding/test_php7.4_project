<?php declare(strict_types=1);

namespace Console;

class Factory
{
	/**
	 * @param array $args
	 * @param \Closure $closure
	 * @throws \Exception
	 */
	public function find(array $args, \Closure $closure): void
	{
		foreach ($closure() as $file) {
			if ($file->isFile()) {
				$fileData = file_get_contents($file->getPathname());
				$fileName = $file->getFilename();
				$exName = explode(".", $fileName);
				if ($class = $this->getCurrentService(current($exName))) {
					$class->setCommandList(explode(",", $fileData));
					echo $class->getCurrentCommandList($args);
				}
			}
		}
	}

	/**
	 * @param string $className
	 * @return object
	 * @throws \Exception
	 */
	protected function getCurrentService(string $className): object
	{
		$className = "\\Console\\Service\\" . $className;
		if (class_exists($className, true)) {
			return new $className;
		}

		throw new \Exception('Class not found');
	}
}