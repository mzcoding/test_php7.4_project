<?php declare(strict_types=1);

namespace Console\Service;

use Console\Contract\TransportInterface;

class Car extends Transport implements TransportInterface
{
	public string $name = 'car';
}