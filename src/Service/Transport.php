<?php declare(strict_types=1);

namespace Console\Service;


use Console\Contract\TransportInterface;

class Transport implements TransportInterface
{
	/**
	 * @var string
	 */
	public string $name = 'general';

	/**
	 * @var array|string[]
	 */
	protected array $accessCommandList = [
   	   'color', 'model'
    ];

	/**
	 * @param array ...$list
	 */
	public function setCommandList(array ...$list): void
	{
		$this->accessCommandList = $list[0];
	}


	/**
	 * @param array $args
	 * @return string
	 */
	public function getCurrentCommandList(array $args = []): string
	{
		$response = "";
		foreach($args as $arg) {
			if(in_array($arg, $this->accessCommandList)) {
				$response .= "Argument -  ". $arg . " in file " . $this->name . "{" . implode(",", $this->accessCommandList) . "}\n\r";
			}else {
				$response .= "Argument -  ". $arg . " in file " . $this->name . " is empty \n\r";
			}
		}

		return $response;
	}
}