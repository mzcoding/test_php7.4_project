<?php declare(strict_types=1);


namespace Console\Service;


use Console\Contract\CommandListInterface;
use Console\Contract\TransportInterface;

class Bike extends Transport
{
	public string $name = 'bike';
}