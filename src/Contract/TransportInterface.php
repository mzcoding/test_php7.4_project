<?php declare(strict_types=1);

namespace Console\Contract;

interface TransportInterface
{
	/**
	 * @param array ...$list
	 */
	public function setCommandList(array ...$list): void;

}