<?php declare(strict_types=1);

namespace Console\Contract;


interface CommandListInterface
{
	/**
	 * @return array
	 */
	public function getCurrentCommandList(): array;
}